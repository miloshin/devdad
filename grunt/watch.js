module.exports = {

  options: {
    spawn: false,
    livereload: true
  },

  scripts: {
    files: [
      'src/scripts/*.js'
    ],
    tasks: [
      'jshint',
      'uglify'
    ]
  },

  styles: {
    files: [
      'src/styles/*.scss',
      'src/styles/abstracts/*.scss',
      'src/styles/base/*.scss',
      'src/styles/components/*.scss',
      'src/styles/layouts/*.scss',
      'src/styles/pages/*.scss',
      'src/styles/themes/*.scss',
      'src/styles/vendors/*.scss'
    ],
    tasks: [
      'sass:dev'
    ]
  },
  html: {
        files: ['index.html'],
        tasks: ['build']
  },
};